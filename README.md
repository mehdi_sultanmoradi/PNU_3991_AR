## PNU_3991_AR
---------
# مهدی سلطانمرادی
 
- [PNU_3991_AR](https://github.com/sultanmoradimehdi/PNU_3991_AR)
- [GitHub](https://github.com/sultanmoradimehdi)
- [Resume]( https://sultanmoradimehdi.github.io/resume/)
- [SOP](https://sultanmoradimehdi.github.io/SOP/)
- [JavaScript.Certificates](java.png)
- [HTML.Certificates](HTML.png)
- [git-patchwork](patchwork.png)
---
### فعالیت گروهی

- [ معرفی درس مشابه در دانشگاه خلیج فارس ](http://smbidoki.ir/crsdetail.php?crsid=41)
- [pepperdine معرفی درس مشابه در دانشگاه](https://seaver.pepperdine.edu/academics/ge/faculty/researchskills.htm)
- [روش پژوهش پایان نامه یک](https://sultanmoradimehdi.github.io/Group-project/ThesisForMethodology.pdf)
- [روش پژوهش پایان نامه دو](https://sultanmoradimehdi.github.io/Group-project/Dependabilityanalysisandrecoverysupportforsmartgrids.pdf)
- [ ارایه کتبی صفحه ۷ تا ۹ LaTex ](https://sultanmoradimehdi.github.io/Group-project/Sultan.7-9.pdf)
------------------
## گروه ها
    
1. RPM 
    1. [_BSc-03_مهدی سلطانمرادی](https://github.com/AliRazavi-edu/PNU_3991/tree/master/_BSc/ResearchAndPresentationMethods/1115133_01/03_%D9%85%D9%87%D8%AF%D9%8A%20%D8%B3%D9%84%D8%B7%D8%A7%D9%86%20%D9%85%D8%B1%D8%A7%D8%AF%D9%8A)   
    1. [_BSc-41_شیوا غلامی منصور](https://github.com/AliRazavi-edu/PNU_3991/tree/master/_BSc/ResearchAndPresentationMethods/1322010_02/41_%D8%B4%D9%8A%D9%88%D8%A7%20%D8%BA%D9%84%D8%A7%D9%85%D9%8A%20%D9%85%D9%86%D8%B5%D9%88%D8%B1) 
    1. [_BSc-22_عاطفه راستگو](https://github.com/AliRazavi-edu/PNU_3991/tree/master/_BSc/ResearchAndPresentationMethods/1322010_02/22_%D8%B9%D8%A7%D8%B7%D9%81%D9%87%20%D8%B1%D8%A7%D8%B3%D8%AA%DA%AF%D9%88)
    1. [_BSc-08_بی تا غفاری](https://github.com/AliRazavi-edu/PNU_3991/tree/master/_BSc/ResearchAndPresentationMethods/1115133_01/08_%D8%A8%D9%8A%20%D8%AA%D8%A7%20%D8%BA%D9%81%D8%A7%D8%B1%D9%8A)
    1. [_BSc-52_متين كاظمي صابر](https://github.com/AliRazavi-edu/PNU_3991/tree/master/_BSc/ResearchAndPresentationMethods/1322010_02/52_%D9%85%D8%AA%D9%8A%D9%86%20%D9%83%D8%A7%D8%B8%D9%85%D9%8A%20%D8%B5%D8%A7%D8%A8%D8%B1)
    1. [_BSc-27_حسین زندیه](https://github.com/AliRazavi-edu/PNU_3991/tree/master/_BSc/ResearchAndPresentationMethods/1322010_01/27_%D8%AD%D8%B3%D9%8A%D9%86%20%D8%B2%D9%86%D8%AF%D9%8A%D9%87)
    
-------------------
## Winter Semester Courses 1399/2020

## درس کارشناسی 
[1115133_01 شیوه ارایه مطالب علمی و فنی](https://github.com/sultanmoradimehdi/PNU_3991_AR/tree/main/technical-and-scientific-contents-presentation-aproach)
<br>
[آزمایشگاه پایگاه داده](https://github.com/sultanmoradimehdi/PNU_3991_AR/tree/main/technical-and-scientific-contents-presentation-aproach)
<br>
[آزمایشگاه سیستم عامل](https://github.com/sultanmoradimehdi/PNU_3991_AR/tree/main/technical-and-scientific-contents-presentation-aproach)
<br>
[آزمایشگاه فیزیک ۱](https://github.com/sultanmoradimehdi/PNU_3991_AR/tree/main/technical-and-scientific-contents-presentation-aproach)
<br>
[شبکه های کامپیوتری](https://github.com/sultanmoradimehdi/PNU_3991_AR/tree/main/technical-and-scientific-contents-presentation-aproach)
<br>
[سیستمهای اطلاعات مدیریت](https://github.com/sultanmoradimehdi/PNU_3991_AR/tree/main/technical-and-scientific-contents-presentation-aproach)
<br>
[آزمایشگاه مدار منطقی](https://github.com/sultanmoradimehdi/PNU_3991_AR/tree/main/technical-and-scientific-contents-presentation-aproach)
<br>
[فیزیک ۲](https://github.com/sultanmoradimehdi/PNU_3991_AR/tree/main/technical-and-scientific-contents-presentation-aproach)
<br>

--------------
## روز و ساعت ارائه دروس

<table style="width:100%">
  <tr>
    <th >16-18</th>
    <th >14-16</th>
    <th >12-14</th>
    <th>10-12</th>
    <th>8-10</th>
    <th>روز</th>
  </tr>
  <tr>
  </tr>
   <tr>
    <th ></th>
    <th ></th>
    <th ></th>
    <th></th>
    <th ><a>شبکه های کامپیوتری 01-1115092</a></th>
    <th>شنبه</th>
  </tr>
   <tr>
    <th ></th>
    <th ></th>
    <th ></th>
    <th ></th>
    <th ></th>
    <th>یک شنبه</th>
  </tr>
   <tr>
     <th ></th>
     <th ></th>
     <th></th>
     <th ><a>فیزیک ۲ 01-1113095</a></th>
     <th ></th>
    <th>دوشنبه</th>
  </tr>
   <tr>
    <th ></th>
    <th ></th>
    <th ></th>
    <th ></th>
    <th ></th>
    <th>سه شنبه</th>
 </tr>
  <tr>
   <th ></th>
   <th ></th>
   <th ></th>
   <th ></th>
   <th ></th>
   <th>چهارشنبه</th>
 </tr>
 <tr>
  <th ></th>
  <th ></th>
  <th ></th>
  <th ><a>آزمایشگاه سیستم عامل 01-1115116</a></th>
  <th ></th>
  <th>پنجشنبه</th>
  </tr>
</table>
